<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package lacerda
 */

get_header();
?>

<?php 
	$my_id = 286;
	$post_id_5369 = get_post($my_id);
	$content = $post_id_5369->post_content;
	$content = apply_filters('the_content', $content);
	$content = str_replace(']]>', ']]>', $content);
	echo $content; ?>

<section class="main-lista-conteudo" id="main">
	<div class="container">
		<div class="col-md-8 col-lg-9 col-xs-12">
			<h1><?php echo single_cat_title( '', false ); ?></h1>
			
			<?php
			if ( have_posts() ) :
				echo '<ul class="conteudo-list">';
				/* Start the Loop */
				while ( have_posts() ) :
					the_post();
					get_template_part( 'template-parts/content', get_post_type() );

				endwhile;
				echo '</ul>'; ?>


				<div class="pagination">
					<?php
						if (function_exists('wp_pagenavi'))
						{
							wp_pagenavi();
						}
					?>
				</div>

			<?php else :
			echo "<h5>Não encontramos resultados</h5>";
			endif;
			?>

		</div>


		<aside class="col-md-4 col-lg-3">

					<?php
					if(is_active_sidebar('sidebar_midia')){
					dynamic_sidebar('sidebar_midia');
					}
					?>
			
		</aside>
	</div>
</section>

<script type="text/javascript">
	jQuery(document).ready(function () {
	    // Handler for .ready() called.
	    jQuery('html, body').animate({
	        scrollTop: jQuery('#main').offset().top
	    }, 'slow');
	});
</script>
<?php
//get_sidebar();
get_footer();
