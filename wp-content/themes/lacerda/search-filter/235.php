<?php
/**
 * Search & Filter Pro 
 *
 * Sample Results Template
 * 
 * @package   Search_Filter
 * @author    Ross Morsali
 * @link      https://searchandfilter.com
 * @copyright 2018 Search & Filter
 * 
 * Note: these templates are not full page templates, rather 
 * just an encaspulation of the your results loop which should
 * be inserted in to other pages by using a shortcode - think 
 * of it as a template part
 * 
 * This template is an absolute base example showing you what
 * you can do, for more customisation see the WordPress docs 
 * and using template tags - 
 * 
 * http://codex.wordpress.org/Template_Tags
 *
 */

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}		

	global $searchandfilter;
	$sf_current_query = $searchandfilter->get(235)->current_query();


	$args = array(
		"str" 					=> '%2$s', 
		"delim" 				=> array(", ", " - "), 
		"field_delim"				=> ', ', 
		"show_all_if_empty"			=> false 
	);
	
	$nome = $sf_current_query->get_fields_html(
		array("_sft_filtro"), 
		$args
	);
	echo '<div class="descricao">';
	echo "<h1>";
	echo $nome ;
	echo '</h1> <div class="info">';
	$slug = strtolower(clean($nome));

	$term = get_term_by('slug', $slug, 'filtro'); 
	echo get_field('descricao', $term);

	echo '</div></div>	';

if ( $query->have_posts() )
{
	?>
	
	<ul class="conteudo-list">
	<?php
	while ($query->have_posts())
	{
		$query->the_post();
		
    	$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
		echo '<li>
                    <a href="'.get_the_permalink().'" class="img" style="background-image:url('.$image[0].');">
                    </a>
                    <div class="meta">
                        <h5>
                            '.get_the_date().'             
                        </h5>
                        <ul class="blog-categories">';
                            $categories = wp_get_post_categories( get_the_ID() );
                            //loop through them
                            foreach($categories as $c){
                              $cat = get_category( $c );
                              //get the name of the category
                              $cat_id = get_cat_ID( $cat->name );
                              //make a list item containing a link to the category
                              echo '<li><a href="'.get_category_link($cat_id).'" title="'.$cat->name.'" class="'.$cat->slug.'">'.$cat->name.'</a></li>';
                            }
                            echo '
                        </ul>            
                    </div>
                    <div class="info">
                        <h3>
                            <a href="'.get_the_permalink().'">'.get_the_title().'</a>
                        </h3>
                        <p>
                            <a href="'.get_the_permalink().'">'.excerpt(30).'</a>
                        </p> ';
                        $responsavel = get_the_terms( $post->ID , 'responsavel' ); 
                        if( $responsavel ):
                        foreach ( $responsavel as $resp ) { 
                            echo '<div class="responsavel">
                                <div class="img" style="background-image:url('.get_field('foto', $resp).')">
                                </div>
                                <div>
                                    <h5><span>Por:</span> '.$resp->name.'</h5>
                                    <span>'.get_field('posicao', $resp).'</span>
                                </div>
                            </div>';
                        }
                        endif;

                        echo '<div class="vc_btn3-container">
                            <a class="vc_btn3 vc_btn3-shape-square vc_btn3-style-flat" href="'.get_the_permalink().'" title="ler conteúdo">ler conteúdo</a>
                        </div>
                    </div>
                </li>';
	}
	?>
	</ul>
	<div class="pagination"><?php
			/* example code for using the wp_pagenavi plugin */
			if (function_exists('wp_pagenavi'))
			{
				wp_pagenavi( array( 'query' => $query ) );
			}
		?>
	</div>
	<?php
}
else
{
	echo "<h5>Não encontramos resultados</h5>";
}
?>