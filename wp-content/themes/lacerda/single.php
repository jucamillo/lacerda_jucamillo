<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package lacerda
 */

get_header(); 

while (have_posts()): the_post(); 

$post_type = get_post_type(); ?>

<section class="single-top">
	<div class="container">
		<div class="col-lg-6">
			<figure>
				<?php lacerda_post_thumbnail(); ?>
			</figure>
		</div>
		<div class="col-lg-5 ">
			<div class="bread_title">
				<?php  if($post_type == 'na_midia'): ?>
					<a href="<?php echo get_home_url(); ?>/na-midia" title="Voltar" class="back">
						<img src="<?php echo get_template_directory_uri(); ?>/images/back.svg" alt="Voltar">
						<h3>Voltar</h3>
					</a>
				<?php else: ?>
					<a href="<?php echo get_home_url(); ?>/conteudo" title="Voltar" class="back">
						<img src="<?php echo get_template_directory_uri(); ?>/images/back.svg" alt="Voltar">
						<h3>Voltar</h3>
					</a>
				<?php endif; ?>
	 			<?php echo do_shortcode( '[custom_breadcrumbs]' ); ?>
			</div>

			<?php $responsavel = get_the_terms( $post->ID , 'responsavel' ); 
            if( $responsavel ):
            foreach ( $responsavel as $resp ) { 
                echo '<div class="responsavel">
                    <a href="'.get_home_url().'/conteudo/autor/'.$resp->slug.'" title="'.$resp->name.'" class="img" style="background-image:url('.get_field('foto', $resp).')">
                    </a>
                    <a href="'.get_home_url().'/conteudo/autor/'.$resp->slug.'" title="'.$resp->name.'">
                        <h4><span>Por:</span> '.$resp->name.'</h4>
                    </a>
                </div>';
            }
            endif; ?>

			<h1><?php the_title(); ?></h1>

			<?php the_excerpt(); ?>
		</div>
	</div>
	
</section>
<section class="main-lista-conteudo content-section">
	<div class="container">
		<div class="col-lg-9 col-md-8 col-xs-12">
			<?php echo sharethis_inline_buttons(); ?>

			<article>
				<?php the_content(); ?>



				<?php echo get_the_tag_list('<p><strong>Tags:</strong> ',', ','</p>'); ?>


                <?php if( get_field('fonte') ): ?>
                	<p>
                		<strong>Fonte:</strong>
                		<?php if( get_field('link') ): ?>
                		<a href="<?php the_field('link'); ?>" title="<?php the_field('fonte'); ?>" target="_blank">
               			<?php endif; ?>
                        
                        	<?php the_field('fonte'); ?>
                		
                		<?php if( get_field('link') ): ?>
                		</a>
               			<?php endif; ?>
                    </p>
                <?php endif; ?>




			</article>

			<div class="share-down">
				<h3>Compartilhe</h3>
				<?php echo sharethis_inline_buttons(); ?>
			</div>
		</div>

		<?php  if($post_type == 'na_midia'): ?>
		<aside class="col-md-4 col-lg-3 col-xs-12">
			<?php
			if(is_active_sidebar('sidebar_midia')){
			dynamic_sidebar('sidebar_midia');
			}
			?>
		</aside>

		<?php else: ?>
		<div class="col-md-4 col-lg-3 col-xs-12">
			<?php get_sidebar(); ?>
			
		</div>
		<?php endif; ?>


	</div>

</section>




<?php endwhile; 
//get_sidebar();
get_footer(); ?>




<script type="text/javascript">
	winHT = jQuery('.ht').innerHeight();

hfWinHT = winHT/2;

trdWinHT = winHT/3;

var scrollPositionC1 = jQuery('.content-section').offset().top - winHT;
var scrollPositionC2 = (jQuery('.contato-sec').offset().top - winHT) - 347;

jQuery(window).on('scroll', function(){

    if (jQuery(window).scrollTop() > scrollPositionC1) {
    	//alert('top')
        jQuery('.content-section .col-lg-9 > .sharethis-inline-share-buttons').addClass('fixed');
    } else{
        jQuery('.content-section .col-lg-9 > .sharethis-inline-share-buttons').removeClass('fixed');

    }
    if (jQuery(window).scrollTop() > scrollPositionC2) {
    	//alert('top')
        jQuery('.content-section .col-lg-9 > .sharethis-inline-share-buttons').addClass('down');
    } else{
        jQuery('.content-section .col-lg-9 > .sharethis-inline-share-buttons').removeClass('down');

    }
});


jQuery('img.aligncenter').parent().css('text-align', 'center');
jQuery('img.aligncenter').parent().css('margin', '45px 0');
</script>