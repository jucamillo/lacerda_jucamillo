<form action="<?php echo home_url( '/' ); ?>" role="search" method="get" class="search-form">
    <input type="text" name="s" id="search" value="<?php the_search_query(); ?>" placeholder="Busque aqui" />
    <input type="hidden" name="post_type[]" value="na_midia" />
    <button><img src="<?php echo get_template_directory_uri();?>/images/search.svg"></button>
</form>