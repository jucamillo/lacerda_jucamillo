<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package lacerda
 */

get_header();
?>
<?php while ( have_posts() ) : the_post(); ?>
	<section class="miolo-atuacao">
		<div class="container">
			<div class="col-xs-12">
				<a href="<?php echo get_home_url(); ?>/atuacao" title="Voltar" class="back">
					<img src="<?php echo get_template_directory_uri(); ?>/images/back.svg" alt="Voltar">
					<h3>Voltar</h3>
				</a>
	 			<?php echo do_shortcode( '[custom_breadcrumbs]' ); ?>
			</div>
			<div class="col-md-7 col-lg-6 col-xs-12">
                <div class="titulo">
                    <img src="<?php echo get_field('icone'); ?>" alt="<?php echo get_the_title(); ?>">
                    <h1>
                    <?php  the_title(); ?>
                    </h1>
                </div>
                <div class="content">
					<?php the_content(); ?>
 
					<?php
                    $responsavel = get_the_terms( $post->ID , 'responsavel' ); 
                        if( $responsavel ):
                        foreach ( $responsavel as $resp ) { 

                        echo '<h3>'.get_field('posicao', $resp).'</h3>';
                            echo '<div class="responsavel">
                                 <a href="'.get_home_url().'/conteudo/autor/'.$resp->slug.'" title="'.$resp->name.'"  class="img" style="background-image:url('.get_field('foto', $resp).')">
                                </a>
                                 <a href="'.get_home_url().'/conteudo/autor/'.$resp->slug.'" title="'.$resp->name.'" >
                                    <h5>'.$resp->name.'</h5>
                                    <span>'.get_field('posicao', $resp).'</span>
                                </a>
                            </div>';
                        }
                        endif;
                        ?>
                </div>
			</div>
			<div class="col-md-5 col-lg-6  col-xs-12 lam">
				<?php
				    if ( have_rows('lamina') ):
				        echo '<div class="laminas"><div class="border">
				        		<h3>
				        			<img src="'.get_template_directory_uri().'/images/lamina.svg" alt="Lâminas">
				        			Lâminas
				        		</h3>
						        <div class="owl-carousel laminas-owl">';

    							$count = 0;
						            while ( have_rows('lamina') ) : the_row();
						                if ($count == 0) {
										    echo "<ul>";
										}
        								$count++;
						                echo '<li>
						                    <a href="'.get_sub_field('arquivo').'" target="_blank" title="'.get_sub_field('nome').'">
						                        <img src="'.get_sub_field('thumbnail').'" alt="'.get_sub_field('nome').'">
						                        <div class="info">
						                        <h5>'.get_sub_field('nome').'</h5>
						                        <div class="btn">Baixar</div>
						                        </div>
						                    </a>
						                </li>';
						                if ($count == 2) {
										    $count = 0;
										    echo "</ul>";
										}
						            endwhile;
						        $string .= '</div></div></div>';
						    endif;

						    ?>
			</div>
		</div>
	</section>

	<section>
		<div class="container">
			<div class="col-xs-12">
				<h1>
					Outras expertises
				</h1>
				
	 				<?php echo do_shortcode( '[atuacao_all]' ); ?>
			</div>
		</div>
	</section>
	<section class="related">
		<div class="container">
			<div class="col-xs-12">
				<h1>
					Conteúdo relacionado
				</h1>
				<?php echo do_shortcode( '[related_atuacao atuacao="'.get_the_title().'"]' ); ?>

			</div>
		</div>
	</section>



<?php endwhile;  ?>


<?php
get_footer();
