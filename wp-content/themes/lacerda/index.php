<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package lacerda
 */

get_header();
?>

<?php 
$my_id = 275;
$post_id_5369 = get_post($my_id);
$content = $post_id_5369->post_content;
$content = apply_filters('the_content', $content);
$content = str_replace(']]>', ']]>', $content);
echo $content; ?>

<section class="main-lista-conteudo">
	<div class="container">
		<div class="col-md-8 col-lg-9 col-sm-12 col-xs-12">
			
			<h1>
				Confira nossos conteúdos
			</h1>
			<?php
			if ( have_posts() ) :
				echo '<ul class="conteudo-list">';
				/* Start the Loop */
				while ( have_posts() ) :
					the_post();
					get_template_part( 'template-parts/content', get_post_type() );

				endwhile;
				echo '</ul>'; ?>


				<div class="pagination">
					<?php
						if (function_exists('wp_pagenavi'))
						{
							wp_pagenavi();
						}
					?>
				</div>

			<?php else :
			echo "<h5>Não encontramos resultados</h5>";
			endif;
			?>

		</div>


		<div class="col-md-4 col-lg-3 col-sm-12 col-xs-12">
			<?php get_sidebar(); ?>
			
		</div>
	</div>
</section>

<?php 
$my_id = 282;
$post_id_5369 = get_post($my_id);
$content = $post_id_5369->post_content;
$content = apply_filters('the_content', $content);
$content = str_replace(']]>', ']]>', $content);
echo $content; ?>
<?php
get_footer();
