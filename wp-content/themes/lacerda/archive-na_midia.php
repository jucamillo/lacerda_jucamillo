<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package lacerda
 */

get_header();
?>

<?php 
	$my_id = 286;
	$post_id_5369 = get_post($my_id);
	$content = $post_id_5369->post_content;
	$content = apply_filters('the_content', $content);
	$content = str_replace(']]>', ']]>', $content);
	echo $content; ?>

<section class="main-lista-conteudo" id="main">
	<div class="container">
		<div class="col-xs-12">
			
		</div>
		<div class="col-md-8 col-lg-9 col-xs-12">
			<div class="filtro-midia">
				<h3>Filtre sua exibição</h3>
		 		<?php echo do_shortcode( '[searchandfilter id="294"]' ); ?>
			</div>
			<?php
			if ( have_posts() ) :
				echo '<ul class="conteudo-list">';
				/* Start the Loop */
				while ( have_posts() ) :
					the_post();
					get_template_part( 'template-parts/content', get_post_type() );

				endwhile;
				echo '</ul>'; ?>


				<div class="pagination">
					<?php
						if (function_exists('wp_pagenavi'))
						{
							wp_pagenavi();
						}
					?>
				</div>

			<?php else :
			echo "<h5>Não encontramos resultados</h5>";
			endif;
			?>

		</div>


		<aside class="col-md-4 col-lg-3 col-xs-12">
			<section id="search-form-wid" class="widget widget_search">
				<form action="<?php echo home_url( '/' ); ?>" role="search" method="get" class="search-form">
				    <input type="text" name="s" id="search" value="<?php the_search_query(); ?>" placeholder="Busque aqui" />
				    <input type="hidden" name="post_type[]" value="na_midia" />
				    <button><img src="<?php echo get_template_directory_uri();?>/images/search.svg"></button>
				</form>
			</section>
					<?php
					if(is_active_sidebar('sidebar_midia')){
					dynamic_sidebar('sidebar_midia');
					}
					?>
			
		</aside>
	</div>
</section>

			
<?php


global $searchandfilter;
	$sf_current_query = $searchandfilter->get(294)->current_query();
	if($sf_current_query->is_filtered()):
	?>
<script type="text/javascript">
	jQuery(document).ready(function () {
	    // Handler for .ready() called.
	    jQuery('html, body').animate({
	        scrollTop: jQuery('#main').offset().top
	    }, 'slow');
	});
</script>
<?php endif;?>

<?php
//get_sidebar();
get_footer();
