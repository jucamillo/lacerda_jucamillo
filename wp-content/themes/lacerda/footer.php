<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package lacerda
 */

?>

	</section>

	<?php if(!is_page(15)):?>
	<section class="contato-sec">
		<div class="container">
			<div class="col-md-6">

                <?php if( get_field('titulo_contato', 'option') ): ?>
                	<h1>
                        <?php the_field('titulo_contato', 'option'); ?>
                    </h1>
                <?php endif; ?>

                <?php if( get_field('texto_contato', 'option') ): ?>
                	<p>
                        <?php the_field('texto_contato', 'option'); ?>
                    </p>
                <?php endif; ?>

			</div>
			<div class="col-md-6">
				<ul>
				<?php if( get_field('telefone', 'option') ): ?>
                	<li>
	                	<h5>
	                		Ligue para nós
	                	</h5>
	                	<h3>
	                		<a href="tel:+55<?php the_field('telefone', 'option'); ?>" target="_blank" title="Ligue para nós">
	                			<?php the_field('telefone', 'option'); ?>
	                		</a>
	                    </h3>
	                </li>
                <?php endif; ?>
				<?php if( get_field('email', 'option') ): ?>
                	<li class="mail">
	                	<h5>
	                		envie um email
	                	</h5>
	                	<h3>
	                		<a href="mailto:<?php the_field('email', 'option'); ?>" target="_blank" title="envie um email">
	                			<?php the_field('email', 'option'); ?>
	                		</a>
	                    </h3>
	                </li>
                <?php endif; ?>
				<?php if( get_field('shortcode', 'option') ): ?>
                	<li>
	                	<h5>
	                		ou mande-nos uma mensagem
	                	</h5>
	                	<?php the_field('shortcode', 'option'); ?>
	                </li>
                <?php endif; ?>
				</ul>
			</div>
		</div>
	</section>
<?php endif; ?>
	<footer id="colophon" class="site-footer">
		<div class="container">
			<div class="col-lg-3">

				<?php if( get_field('logo', 'option') ): ?>
					<a href="<?php echo get_home_url();?>" title="Lacerda Santana Advocacia" class="logo">
	                	<img src="<?php the_field('logo', 'option'); ?>" alt="Lacerda Santana Advocacia">
	                </a>
                <?php endif; ?>


				<?php if( get_field('copyright', 'option') ): ?>
					<?php the_field('copyright', 'option'); ?>
				<?php endif; ?>
			</div>
			<div class="col-md-6">
				<?php
				wp_nav_menu( array(
					'theme_location' => 'menu-2',
				) );
				?>
			</div>
			<div class="col-lg-2">
				<?php if( get_field('formulario_newsletter', 'option') ): ?>
					<?php the_field('formulario_newsletter', 'option'); ?>
				<?php endif; ?>
			</div>
			<div class="col-lg-1">
				<?php if( get_field('qualitare', 'option') ): ?>
					<a href="https://www.qualitare.com/" target="_blank" title="Qualitare" class="qualitare-logo">
						<img src="<?php the_field('qualitare', 'option'); ?>" alt="Qualitare">
					</a>
                <?php endif; ?>
				
			</div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

<script type="text/javascript">
	

jQuery(function(){
    var hasBeenTrigged = false;
    jQuery(window).scroll(function() {
        if (jQuery(this).scrollTop() >= 50 && !hasBeenTrigged) { // if scroll is greater/equal then 100 and hasBeenTrigged is set to false.
             jQuery('header').addClass('scroll');
            hasBeenTrigged = true;
        } else if(jQuery(this).scrollTop() < 50 && hasBeenTrigged){
             jQuery('header').removeClass('scroll');
            hasBeenTrigged = false;

        }
    });
    jQuery("ul.conteudos li:first-child").addClass('active');


    jQuery("ul.conteudos li h3").hover(function(){
    	jQuery("ul.conteudos li").removeClass('active');
        jQuery(this).parent().addClass('active');
    },function(){

    });

    //jQuery('.searchandfilter> ul>li>ul').append('<li><a href="../conteudo" title="Todos">Todos</a></li>');

	jQuery('.filtro-midia .searchandfilter > ul > li.sf-field-post-meta-ano >ul > li.sf-option-active label').clone().insertBefore('.filtro-midia .searchandfilter > ul > li.sf-field-post-meta-ano > ul');
    jQuery('.filtro-midia .searchandfilter > ul > li.sf-field-post-meta-ms >ul > li.sf-option-active label').clone().insertBefore('.filtro-midia .searchandfilter > ul > li.sf-field-post-meta-ms > ul');



});


jQuery( document ).ajaxComplete(function() {
    jQuery('#filtro .searchandfilter> ul>li>ul').append('<li class="all"><a href="../conteudo" title="Todos">Todos</a></li>');
});
jQuery('#banner-home .wpb_image_grid .wpb_image_grid_ul').addClass('owl-carousel');


jQuery('#banner-home .wpb_image_grid .wpb_image_grid_ul').owlCarousel({
    margin:0,
    responsiveClass:true,
    dots: false,
    nav:false,
    autoHeight:true,
    autoplay: true,
    autoplayTimeout: 5000,
    animateOut: 'fadeOut',
    items:1,
    loop: true
})

jQuery('.laminas-owl').owlCarousel({
    margin:0,
    responsiveClass:true,
    dots: true,
    nav:false,
    autoHeight:false,
    autoplay: false,
    autoplayTimeout: 5000,/*
    animateOut: 'fadeOut',*/
    items:1,
    loop: true
})
jQuery('.conteudos.interna').owlCarousel({
    margin:0,
    responsiveClass:true,
    dots: true,
    nav:false,
    autoHeight:false,
    autoplay: false,
    autoplayTimeout: 5000,/*
    animateOut: 'fadeOut',*/
    items:1,
    loop: true
})



jQuery(document).delegate('.filtro-midia .searchandfilter > ul > li > label', 'click', function(event) {
    event.preventDefault();
    jQuery(this).parent().toggleClass('active');
});




















if( jQuery(window).width() < 1500 ){
    jQuery('body.home ul.atuacao').addClass('owl-carousel');

    setTimeout( function() {
        jQuery('body.home ul.atuacao').owlCarousel({
            loop:true,
            margin:0,
            responsiveClass:true,
            dots: true,
            nav:false,
            autoHeight:true,
            autoplay: false,
            autoplayTimeout: 10000,
            responsive:{
                0:{
                    items:1,
                    stagePadding: 30,
                    margin:2
                },
                373:{
                    items:1,
                    stagePadding: 30,
                    margin:5
                },
                500:{
                    items:1,
                    stagePadding: 80,
                    margin:5
                },
                767:{
                    items:2,
                    stagePadding: 60,
                    margin:5
                },
                991:{
                    items:3,
                    stagePadding: 30,
                    margin:5
                },
                1199:{
                    items:3,
                    stagePadding: 100,
                    margin:5
                },
                1400:{
                    items:4,
                    stagePadding: 25,
                    margin:5
                }
            }
        })
    }, 600);

} 


if( jQuery(window).width() < 768 ){
    setTimeout( function() {
		jQuery('div#filtro .searchandfilter ul li.sf-field-taxonomy-filtro li.sf-option-active label').clone().insertBefore('div#filtro .searchandfilter ul li.sf-field-taxonomy-filtro > ul');


		jQuery(document).delegate('div#filtro .searchandfilter > ul > li > label', 'click', function(event) {
		    event.preventDefault();
		    jQuery(this).parent().toggleClass('active');
		});

    }, 600);

} 





if( jQuery(window).width() < 992 ){
    setTimeout( function() {
		jQuery('.main-lista-conteudo aside .widget .search-form').insertBefore('.main-lista-conteudo .col-md-8 h1');



		jQuery('.main-lista-conteudo aside .widget .search-form').insertBefore('.main-lista-conteudo .col-md-8 .filtro-midia');


		

		jQuery('#confira-mais').insertBefore('.main-lista-conteudo .col-md-4');


    }, 600);

} 



if( jQuery(window).width() < 768 ){
    var alturaHeader = jQuery('header').height();
    var alturaBanner = jQuery('section#banner-home').height();
    jQuery('body.home header ul.social').css('height', alturaBanner + alturaHeader + 20 + 'px');
}
























<?php if(is_page(11)):?>

jQuery('div#wpsl-wrap > *:nth-child(1)').prepend('<h1>Onde nos encontrar</h1>');

jQuery('#wpsl-search-wrap form > .wpsl-input').append('<div class="select-state">Escolher um estado</div>');

//jQuery('#wpsl-search-wrap .wpsl-search-btn-wrap').append('Pesquise Aqui');

//jQuery('div#wpsl-wrap > .wpsl-search').append('<div class="closest">Encontrar unidade mais próxima</div>');

jQuery('#wpsl-search-wrap form > .wpsl-input > input').attr('placeholder', 'Pesquise aqui');
//jQuery('#wpsl-search-wrap form > .wpsl-input > input').addClass('zero');
jQuery(document).delegate('#wpsl-search-wrap form > .wpsl-input > div.select-state', 'click', function(event) {
    
    jQuery('#wpsl-checkbox-filter').toggleClass('active');

});


jQuery(document).delegate('#wpsl-search-wrap form #wpsl-checkbox-filter > li label', 'click', function(event) {
    jQuery(this).parent().siblings().find('input').prop('checked', false);
    i = jQuery(this).html();

    values=i.split('>');
    two=values[1];
    cid = two.split('-');
    cidade = cid[0];
    if( !jQuery('#wpsl-search-wrap form input#wpsl-search-input').val() ) {

        jQuery('#wpsl-search-wrap form input#wpsl-search-input').val(cidade);
    }

    if (jQuery('#wpsl-search-wrap form input#wpsl-search-input').val().match(/^\d/)) {
       // Return true
    } else{
        jQuery('#wpsl-search-wrap form input#wpsl-search-input').val(cidade);
        
    }

    setTimeout( function() {
        jQuery('div#wpsl-wrap div#wpsl-result-list').addClass('active');
        jQuery('#wpsl-search-wrap input#wpsl-search-btn').trigger('click');
    
    }, 600); 
});

/*
jQuery(document).delegate('#wpsl-search-wrap .wpsl-search-btn-wrap:not(.click)', 'click', function(event) {
    jQuery('div#wpsl-wrap div#wpsl-result-list').toggleClass('active');
});

jQuery(document).delegate('#wpsl-search-wrap .wpsl-search-btn-wrap.click', 'click', function(event) {
    jQuery('#wpsl-search-wrap form #wpsl-checkbox-filter > li label input').prop('checked', false);

    setTimeout( function() {
        jQuery('#wpsl-search-wrap input#wpsl-search-btn').trigger('click');
        jQuery('div#wpsl-wrap div#wpsl-result-list').removeClass('active');
        jQuery('#wpsl-search-wrap .wpsl-search-btn-wrap').removeClass('click');
        jQuery('#wpsl-checkbox-filter').removeClass('active');
    
    }, 600); 
});
*/
jQuery( "#wpsl-search-wrap form > .wpsl-input > input" ).change(function() {
    jQuery('#wpsl-search-wrap form #wpsl-checkbox-filter > li label input').prop('checked', false);

    setTimeout( function() {
        jQuery('#wpsl-search-wrap input#wpsl-search-btn').trigger('click');
        jQuery('div#wpsl-wrap div#wpsl-result-list').addClass('active');
       // jQuery('#wpsl-search-wrap .wpsl-search-btn-wrap').removeClass('click');
       // jQuery('#wpsl-checkbox-filter').removeClass('active');
    
    }, 600); 
});


jQuery(document).delegate('div#wpsl-wrap > .wpsl-search .closest', 'click', function(event) {
    jQuery(this).parent().siblings().find('input').prop('checked', false);
    setTimeout( function() {
        jQuery('#wpsl-search-wrap input#wpsl-search-btn').trigger('click');
        jQuery('div#wpsl-wrap div#wpsl-result-list').addClass('active');
    
    }, 600); 
});



jQuery(document).mouseup(function(e) 
{
    var container = jQuery("div#wpsl-wrap div#wpsl-result-list.active");

    // if the target of the click isn't the container nor a descendant of the container
    if (!container.is(e.target) && container.has(e.target).length === 0) 
    {
        jQuery('div#wpsl-wrap div#wpsl-result-list.active').removeClass('active');
    }
});
<?php endif; ?>

</script>
</body>
</html>
