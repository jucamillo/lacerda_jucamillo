<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package lacerda
 */

    $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );

    if ( !is_singular() ) :

    	$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
		echo '<li>
                    <a href="'.get_the_permalink().'" class="img" style="background-image:url('.$image[0].');">
                    </a>
                    <div class="meta">
                        <h5>
                            '.get_the_date().'             
                        </h5>
                        <ul class="blog-categories">';
                            $categories = wp_get_post_categories( get_the_ID() );
                            //loop through them
                            if($categories):
                                foreach($categories as $c){
                                  $cat = get_category( $c );
                                  //get the name of the category
                                  $cat_id = get_cat_ID( $cat->name );
                                  //make a list item containing a link to the category
                                  echo '<li><a href="'.get_category_link($cat_id).'" title="'.$cat->name.'" class="'.$cat->slug.'">'.$cat->name.'</a></li>';
                                }
                            endif;
                            echo '
                        </ul>            
                    </div>
                    <div class="info">
                        <h3>
                            <a href="'.get_the_permalink().'">'.get_the_title().'</a>
                        </h3>
                        <p>
                            <a href="'.get_the_permalink().'">'.excerpt(30).'</a>
                        </p> ';
                        $responsavel = get_the_terms( $post->ID , 'responsavel' ); 
                        if( $responsavel ):
                        foreach ( $responsavel as $resp ) { 
                            echo '<div class="responsavel">
                                <a href="'.get_home_url().'/conteudo/autor/'.$resp->slug.'" title="'.$resp->name.'" class="img" style="background-image:url('.get_field('foto', $resp).')">
                                </a>
                                <a href="'.get_home_url().'/conteudo/autor/'.$resp->slug.'" title="'.$resp->name.'">
                                    <h5><span>Por:</span> '.$resp->name.'</h5>
                                    <span>'.get_field('posicao', $resp).'</span>
                                </a>
                            </div>';
                        }
                        endif;

                        echo '<div class="vc_btn3-container">
                            <a class="vc_btn3 vc_btn3-shape-square vc_btn3-style-flat" href="'.get_the_permalink().'" title="ler conteúdo">ler conteúdo</a>
                        </div>
                    </div>
                </li>';
            endif; ?>