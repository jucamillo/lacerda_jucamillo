<?php 
    $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
<li class="single">
					<div class="img" style="background-image:url('<?php echo $image[0]; ?>');">
						<ul class="contacts">
			                <?php if( get_field('linkedin') ): ?>
			                	<li>
			                		<a href="<?php the_field('linkedin'); ?>" target="_blank" title="Confira meu linkedin">
			                			<img src="https://homolog.jucamillo.com.br/lacerda/wp-content/uploads/2020/07/linkedin-wt.svg" alt="linkedin">
			                		</a>
			                    </li>
			                <?php endif; ?>
			                <?php if( get_field('email') ): ?>
			                	<li>
			                		<a href="mailto:<?php the_field('email'); ?>" target="_blank" title="Envie um e-mail">
			                			<img src="https://homolog.jucamillo.com.br/lacerda/wp-content/uploads/2020/07/email.svg" alt="email">
			                		</a>
			                    </li>
			                <?php endif; ?>
			                <?php if( get_field('telefone') ): ?>
			                	<li>
			                		<a href="tel:<?php the_field('telefone'); ?>" target="_blank" title="Fale por telefone">
			                			<img src="https://homolog.jucamillo.com.br/lacerda/wp-content/uploads/2020/07/fone.svg" alt="telefone">
			                		</a>
			                    </li>
			                <?php endif; ?>
						</ul>
					</div>
					<div class="info">
						<h3>
							<?php the_title(); ?>
						</h3>
		                <?php if( get_field('posicao') ): ?>
		                	<p>
		                		<?php the_field('posicao'); ?>
		                    </p>
		                <?php endif; ?>
					</div>

</li>